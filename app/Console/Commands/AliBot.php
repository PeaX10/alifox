<?php

namespace App\Console\Commands;

use AliseeksApi\Model\PriceRange;
use AliseeksApi\Model\RealtimeSearchItem;
use AliseeksApi\Model\RealtimeSearchRequest;
use AliseeksApi\Model\SearchRequest;
use App\Category;
use App\Jobs\PositionProduct;
use App\Jobs\ScanProduct;
use App\Product;
use App\ProductDetail;
use App\Services\AliSeekApi;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AliBot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alibot:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Alibot';

    private $api;

    private $pagePerCategory = 100;
    private $itemPerPage = 20;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->api = new AliSeekApi();
        $this->pagePerCategory = env('ALIBOT_PAGE_PER_CAT', $this->pagePerCategory);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('AliBot will start');

        // Product listing
        $this->output->section('Listing product needed to be updated');
        $this->browser();

        $this->output->success('All products as updated');

        $this->output->section('Setting position in global rank');
        $this->ranking();
        $this->output->success('Ranking Finish');
    }

    private function browser()
    {
        $categories = Category::where('original_id', '>', 0)->get();

        foreach($categories as $category){
            $this->output->block($category->name.'\'s products');
            $this->output->text(Carbon::now()->format('d/M/Y H:m:s'));
            $bar = $this->output->createProgressBar($this->itemPerPage*$this->pagePerCategory);
            $bar->start();
            $cpt = 0;
            for($i = 0; $i < $this->pagePerCategory; $i++){
                $options = [
                    'category' => $category->original_id,
                    'priceRange' => new PriceRange(['min' => 1]),
                    'sort' => RealtimeSearchRequest::SORT_NUMBER_OF_ORDERS,
                    'skip' => $i*$this->itemPerPage
                ];

                $products = $this->api->searchRealtime($options);
                if(!is_null($products)){
                    foreach($products->getItems() as $product){
                        if(
                            $product->getOrders() >= 200 && $product->getOrders() <= 15000 &&
                            $product->getRatings() >= 4 && $this->minPrice($product) > 1
                        ){
                            $cpt++;
                            $product['category'] = $category->original_id;
                            $this->updateProduct($product);
                        }else{
                            //$this->output->comment($product->getId() . ' ne correspond pas aux critères');
                        }

                        $bar->advance();
                    }
                }else{
                    $this->output->error('Invalid request');
                }
            }

            $bar->finish();
            $this->output->newLine(1);
            $this->info($cpt.' '.$category->name.'\'s products has been queued');

        }
    }

    private function minPrice($product)
    {
        $min = 9999999999;
        foreach($product->getPriceOptions() as $priceOption){
            if(
                $priceOption->getAmount()->getCurrency() == "USD" &&
                $priceOption->getAmount()->getValue() < $min
            ){
                $min = $priceOption->getAmount()->getValue();
            }
        }
        return $min;
    }

    private function updateProduct($product)
    {
        dispatch(new ScanProduct($product));
    }

    private function ranking(){
        dispatch(new PositionProduct());
    }
}
