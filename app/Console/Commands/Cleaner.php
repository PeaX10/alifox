<?php

namespace App\Console\Commands;

use App\Product;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class Cleaner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleaner:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleaner will clean every useless data in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Cleaner running...');
        $this->cleaner();
    }

    private function cleaner(){
        $products = Product::where('updated_at', '<', Carbon::now()->subWeek(1))->get();

        $bar = $this->output->createProgressBar(count($products));
        $bar->start();
        foreach($products as $product){
            $this->deleteImage($product);
            $productDetails = $product->productDetails;
            foreach($productDetails as $pD){
                $pD->delete();
            }
            $product->delete();
            $bar->advance();
        }
        $bar->finish();

        $this->output->newLine(2);
        $this->output->success('Useless products has been cleaned ('.count($products).')');
    }

    private function deleteImage(Product $product){
        $retry = false;
        do {
            try{
                $img = Storage::disk('s3')->delete($product->id.'.jpg');
                $retry = false;
            } catch (\Exception $e){
                dump($e->getMessage());
                $retry = true;
                sleep(1);
                continue;
            }
        } while($retry);

    }
}
