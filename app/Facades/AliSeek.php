<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class AliSeek extends Facade{

    protected static function getFacadeAccessor() {
        return 'AliSeek';
    }

    public static function setKey($key){
        echo $key;
    }

}