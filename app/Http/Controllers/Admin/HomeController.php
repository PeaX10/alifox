<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class HomeController extends Controller
{
    public function index(){
        $users = User::all();
        $usersByDays  = User::all()->sortByDesc('created_at')->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('d/m/Y');
        });

        return view('back.index', compact('users', 'usersByDays'));
    }
}
