<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function index(){
        $nbProducts = Product::all()->count();
        $nbUsers = User::all()->count() + 500;
        $fromDate = new Carbon('2019-05-30 12:00');
        $nbDollars = $fromDate->diffInMinutes(Carbon::now()) + 234962;

        $fakeProducts = array();
        for($i = 0; $i < 4; $i++){
            do{
                $randomInt = random_int(1, 28);
            }while(in_array($randomInt, $fakeProducts));

            $fakeProducts[] = $randomInt;
        }

        return view('home', compact('nbUsers', 'nbProducts', 'nbDollars', 'fakeProducts'));
    }

    public function goPro(){
        if(Auth::check() && Auth::user()->subscribed('main')) return redirect()->route('home');
        return view('gopro');
    }


    public function terms()
    {
        return view('terms');
    }

    public function privacy()
    {
        return view('privacy');
    }

    public function contact(){
        return view('contact');
    }

    public function affiliates(){
        return view('affiliates');
    }
}
