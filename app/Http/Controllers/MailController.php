<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{

    private $internalEmail = 'alifox.co@gmail.com';

    public function contact(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required|min:50'
        ]);

        Mail::to($this->internalEmail)->send(new Contact($request->input()));

        if (Mail::failures()) {
            return redirect()->back()->with('mail_sended_error', true);
        }else{
            return redirect()->back()->with('mail_sended_success', true);
        }

    }

    public function affiliates(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required|min:50'
        ]);

        Mail::to($this->internalEmail)->send(new Contact($request->input(), 'affiliates'));

        if (Mail::failures()) {
            return redirect()->back()->with('mail_sended_error', true);
        }else {
            return redirect()->back()->with('mail_sended_success', true);
        }

    }
}
