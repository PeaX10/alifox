<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function choosePlan($plan){
        if(Auth::user()->subscribed('main')) return abort(404);
        $plan = Plan::where('slug', $plan)->firstOrFail();
        return view('auth.plan', compact('plan'));
    }

    public function create(Request $request, Plan $plan)
    {
        $request->user()
            ->newSubscription('main', $plan->stripe_plan)
            ->create($request->stripeToken);

        return redirect()->route('home')->with('goPro_success', true);
    }
}
