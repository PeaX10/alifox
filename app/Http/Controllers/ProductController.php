<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    protected $nbPerPage = 15;
    protected $allCategory = 0;

    public function index(Request $request, Category $category){
        if( $request->ajax() ){
            $data = Product::orderBy('order_rate', 'desc');
            if($category->original_id != $this->allCategory){
                $data->where('category', $category->original_id);
            }else{
                $data->where('category', '!=', '509')
                    ->where('category', '!=', '5090301')
                    ->where('category', '!=', '200086021')
                    ->where('category', '!=', '200084017')
                    ->where('category', '!=', '200216959');
            }
            $data->where('updated_at', '>', Carbon::now()->subDays(2));

            if(!$request->user() or (!$request->user()->subscribed('main') && $request->user()->role == 'user')){
                $data = $data->take(3)->get();
                if($request->input('page') > 1) return response()->json([]);
            }else{
                $data = $data->paginate($this->nbPerPage);
                if($data->lastPage() < $request->input('page')){
                    return response()->json([]);
                }
            }
            foreach($data as $k => $v){
                $data[$k]->product_detail = $v->productDetail;
                $data[$k]->image = Storage::disk('s3')->temporaryUrl($v->original_id.'.jpg', Carbon::now()->addMinutes(5));
            }
            return response()->json($data);
        }else{
            return abort(403);
        }

    }

    public function indexReverse(Request $request, Category $category){
        if( $request->ajax() ){
            $data = Product::orderBy('order_rate', 'desc');
            if($category->original_id != $this->allCategory){
                $data->where('category', $category->original_id);
            }else{
                $data->where('category', '!=', '509')
                    ->where('category', '!=', '5090301')
                    ->where('category', '!=', '200086021')
                    ->where('category', '!=', '200084017')
                    ->where('category', '!=', '200216959');
            }
            $data->where('updated_at', '>', Carbon::now()->subDays(2));

            $data = $data->paginate($this->nbPerPage);
            foreach($data as $k => $v){
                $data[$k]->product_detail = $v->productDetail;
                $data[$k]->image = Storage::disk('s3')->temporaryUrl($v->original_id.'.jpg', Carbon::now()->addMinutes(5));
            }

            if(!$request->user() or (!$request->user()->subscribed('main') && $request->user()->role == 'user')){
                $data->put('premium', false);
            }else{
                $data->put('premium', true);
            }
            return response()->json($data);
        }else{
            return abort(403);
        }

    }

    public function getCategories(Request $request){
        if( $request->ajax() ){
            $categories = Category::where('icon', '!=', null)->orderBy('id')->get();
            foreach($categories as $k => $category){
                $categories[$k]->childrens = $category->childrens;
            }
            return response()->json($categories);
        }
    }
}
