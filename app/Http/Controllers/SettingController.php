<?php

namespace App\Http\Controllers;

use App\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public function index(){
        $user = Auth::user();
        return view('settings', compact('user'));
    }

    public function cancelSubscription(){
        $user = Auth::user();
        $user->subscription('main')->cancel();
        return redirect()->back()->with('subscription_cancelled', true);
    }

    public function updateUser(Request $request){
        $rules = [
            'name' => 'unique:users,name,'.$request->user()->id,
            'email' => 'unique:users,email,'.$request->user()->id,
            'password' => 'string|min:8|nullable'
        ];

        if($request->input('password_confirmation')){
            $rules['password_confirmation'] = 'same:password';
        }

        $request->validate($rules);
        $user = $request->user();

        if($request->input('password_confirmation')){
            $request->user()->password = bcrypt($request->password);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect()->back()->with('account_edited', true);
    }

    public function resumeSubscription(){
        $user = Auth::user();
        $user->subscription('main')->resume();
        return redirect()->route('settings')->with('subscription_resumed', true);
    }

    public function swapSubscription($plan){
        $user = Auth::user();
        $plan = Plan::where('slug', $plan)->firstOrFail();
        $user->subscription('main')->swap($plan->stripe_plan);
        return redirect()->back()->with('subscription_swaped', true);
    }

    public function downloadInvoice (Request $request, $invoiceId)
    {
        return $request->user()->downloadInvoice($invoiceId, [
            'vendor' => 'AliFox',
            'product' => 'AliFox - Pro Version',
        ]);
    }
}
