<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $allowedRoles = [
        'admin'
    ];

    public function handle($request, Closure $next)
    {
        if(Auth::check() && in_array(Auth::user()->role, $this->allowedRoles)){
            return $next($request);
        }
        return abort(404);
    }
}
