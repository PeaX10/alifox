<?php

namespace App\Jobs;

use AliseeksApi\Model\RealtimeSearchItem;
use App\Product;
use App\ProductDetail;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ScanProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $product;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(RealtimeSearchItem $product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $product = $this->product;
        if(!Product::where('original_id', $product->getId())->exists()){
            // CREATE
            $p = new Product([
                'category' => $product['category'],
                'original_id' => $product->getId(),
                'image' => $product->getImageUrl()
            ]);
            // Create fake Details
            $idContries = array_rand(Product::COUNTRIES, 2);
            $details = [
                'topCountries' => [
                    Product::COUNTRIES[$idContries[0]],
                    Product::COUNTRIES[$idContries[1]],
                ],
                'competitors' => random_int(0,20)
            ];
            $p->details = json_encode($details);
            // Create image
            $this->uploadImage($product);
            $p->save();
        }else{
            $p = Product::where('original_id', $product->getId())->first();

            // Create fake Details
            $details = json_decode($p->details);
            if(empty($details)) $details = new \stdClass();
            if(empty($details->topCountries)){
                $idContries = array_rand(Product::COUNTRIES, 2);
                $details->topCountries = [
                    Product::COUNTRIES[$idContries[0]],
                    Product::COUNTRIES[$idContries[1]],
                ];
            }
            $details->competitors = random_int(0,20);

            $p->details = json_encode($details);

            // Update Image
            if($p->image != $product->getImageUrl() || Storage::disk('s3')->size($p->original_id.'.jpg') === 0){
                dump($p->id.' : '.$p->original_id.' photo updated');
                $this->uploadImage($product);
                $p->image = $product->getImageUrl();
            }
        }

        $pDetail = new ProductDetail([
            'product_id' => $p->id,
            'title' => $product->getTitle(),
            'ratings' => $product->getRatings(),
            'orders' => $product->getOrders(),
            'price' => $this->minPrice($product)
        ]);
        $pDetail->save();
        $p->product_detail_id = $pDetail->id;

        $prevProductDetail = ProductDetail::where('id', '!=', $pDetail->id)
            ->where('product_id', $p->id)
            ->where('created_at', '<=', Carbon::now()->subHours(6))
            ->orderBy('id', 'desc');
        if($prevProductDetail->exists()){
            $p->order_rate = $this->calOrderRate($prevProductDetail->first(), $pDetail);
        }
        $p->save();
    }

    private function uploadImage($product){
        $retry = false;
        $maxAttempt = 10;
        $currentAttempt = 0;
        do{
            try{
                $img = $product->getImageUrl();
                $img = Image::make($img);
                $img->resize(null, 450, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                Storage::disk('s3')->put($product->getId().'.jpg', $img->stream());
                $retry = false;
            } catch (\Exception $e){
                $retry = true;
                dump($e->getMessage());
                $currentAttempt++;
                sleep(1);
                continue;
            }
        } while($retry && $currentAttempt < $maxAttempt);
    }



    private function calOrderRate(ProductDetail $prevProductDetail, ProductDetail $currentProductDetail)
    {
        $diffInSeconds = $prevProductDetail->created_at->diffInSeconds($currentProductDetail->created_at);
        $diffOrders = $currentProductDetail->orders - $prevProductDetail->orders;
        if($diffInSeconds == 0 || $diffOrders == 0){
            return 0;
        }else{
            return ($diffOrders / $diffInSeconds)*60*60;
        }

    }

    private function minPrice($product)
    {
        $min = 9999999999;
        foreach($product->getPriceOptions() as $priceOption){
            if(
                $priceOption->getAmount()->getCurrency() == "USD" &&
                $priceOption->getAmount()->getValue() < $min
            ){
                $min = $priceOption->getAmount()->getValue();
            }
        }
        return $min;
    }
}
