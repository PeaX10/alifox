<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    private $data;
    private $type;

    public function __construct($data, $type='contact')
    {
        $this->data = $data;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = ($this->type == 'affiliates') ? 'Demande d\'affiliation depuis AliFox' : 'Demande de contact depuis AliFox';
        return $this->view('email.contact', ['data' => $this->data])
            ->subject($subject);
    }
}
