<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    const COUNTRIES = ['AU','BL','CA','ES','FR','GE','IN','IR','IT','NZ','SW','UK','US'];

    protected $fillable = [
        'category',
        'original_id',
        'image',
    ];

    public function productDetail(){
        return $this->belongsTo(ProductDetail::class);
    }

    public function productDetails(){
        return $this->hasMany(ProductDetail::class);
    }
}
