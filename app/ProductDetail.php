<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    protected $fillable = [
        'product_id',
        'title',
        'ratings',
        'orders',
        'price'
    ];

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
