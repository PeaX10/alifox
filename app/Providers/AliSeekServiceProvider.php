<?php

namespace App\Providers;

use App\Services\AliSeek;
use Illuminate\Support\ServiceProvider;

class AliSeekServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(AliSeek::class, function ($app) {
            return new AliSeek();
        });
    }
}
