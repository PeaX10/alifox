<?php

namespace App\Services;

use AliseeksApi\Api\ProductsApi;
use AliseeksApi\Api\SearchApi;
use AliseeksApi\Configuration;
use AliseeksApi\Model\BestSellingSearchRequest;
use AliseeksApi\Model\ProductRequest;
use AliseeksApi\Model\RealtimeSearchRequest;
use AliseeksApi\Model\SearchRequest;
use GuzzleHttp\Client;

class AliSeekApi {

    private $productInstance;
    private $searchInstance;

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $config = Configuration::getDefaultConfiguration()->setApiKey('X-API-CLIENT-ID', env('ALISEEK_API_ACCESS_KEY'));
        $this->productInstance = new ProductsApi(
            new Client(),
            $config
        );
        $this->searchInstance = new SearchApi(
            new Client(),
            $config
        );
    }

    public function getProduct($idProduct)
    {
        $productRequest = new ProductRequest();
        $productRequest->setProductId($idProduct);

        try {
            $result = $this->productInstance->getProduct($productRequest);
            return $result;
        } catch (\Exception $e) {
            echo 'Exception when calling ProductsApi->getProduct: ', $e->getMessage(), PHP_EOL;
        }
    }

    public function searchRealtime($options=array()){
        $searchRequest = new RealtimeSearchRequest();
        foreach($options as $k => $v){
            switch($k){
                case 'text':
                    $searchRequest->setText($v);
                    break;
                case 'sort':
                    $searchRequest->setSort($v);
                    break;
                case 'category':
                    $searchRequest->setCategory($v);
                    break;
                case 'priceRange':
                    $searchRequest->setPriceRange($v);
                    break;
                case 'shipToCountry':
                    $searchRequest->setShipToCountry($v);
                    break;
                case 'shipFromCountry':
                    $searchRequest->setShipFromCountry($v);
                    break;
                case 'skip':
                    $searchRequest->setSkip($v);
                    break;
            }
        }

        $attempt = 0;
        do {
            try {
                $results = $this->searchInstance->realtimeSearch($searchRequest);
                return $results;
            } catch (\Exception $e) {
                $attempt++;
                sleep(1);
                echo 'Exception when calling SearchApi->realtimeSearch: ', $e->getMessage(), PHP_EOL;
                continue;
            }
        } while ($attempt < 5);

    }

    public function bestSellingProducts($options=array()){
        $searchRequest = new BestSellingSearchRequest();
        foreach($options as $k => $v){
            switch($k){
                case 'locale':
                    $searchRequest->setLocale($v);
                    break;
                case 'currency':
                    $searchRequest->setCurrency($v);
                    break;
                case 'category':
                    $searchRequest->setCategory($v);
                    break;
                case 'range':
                    $searchRequest->setRange($v);
                    break;
                case 'skip':
                    $searchRequest->setSkip($v);
                    break;
            }
        }

        try {
            $results = $this->searchInstance->searchBestSelling($searchRequest);
            return $results;
        } catch (\Exception $e) {
            echo 'Exception when calling SearchApi->searchBestSelling: ', $e->getMessage(), PHP_EOL;
        }

    }



}