<?php

namespace App\Services;

use AliseeksApi\Api\ProductsApi;
use AliseeksApi\Api\SearchApi;
use AliseeksApi\Configuration;
use AliseeksApi\Model\BestSellingSearchRequest;
use AliseeksApi\Model\ProductRequest;
use AliseeksApi\Model\RealtimeSearchRequest;
use AliseeksApi\Model\SearchRequest;
use App\Ip;
use GuzzleHttp\Client;

class MultiIP {

    const MOD_RANDOM = 'random';
    const MOD_DISTRIBUTE = 'distribute';

    private $availableIps;
    private $tracker;
    private $attempt;

    function __construct(){
        $this->availableIps = $this->availableIps();
        $this->tracker = 0;
        $this->attempt = 0;
    }

    private function randomIp(){
        return Ip::where('available', true)->random(1);
    }

    private function availableIps(){
        return Ip::where('available', true)->get();
    }

    private function unavailableIps(){
        return Ip::where('available', false)->get();
    }

    public function getFile($url, $mod){
        switch($mod){
            case self::MOD_DISTRIBUTE:
                if(count($this->availableIps) > $this->tracker){
                    $this->tracker = 0;
                }else{
                    $this->tracker++;
                }
                $ip = $this->availableIps[$this->tracker];
                break;
            case self::MOD_RANDOM:
                $ip = $this->randomIp();
                break;
            default:
                return false;
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_INTERFACE, $ip->address);
        $this->attempt++;
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }

}