<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReuseBraintree extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('stripe_id');
            $table->string('braintree_id')->nullable()->after('name');
        });

        Schema::table('subscriptions', function ($table) {
            $table->dropColumn('stripe_id');
            $table->dropColumn('stripe_plan');
            $table->string('braintree_id')->after('quantity');
            $table->string('braintree_plan')->after('braintree_id');
        });

        Schema::table('plans', function ($table) {
            $table->renameColumn('stripe_plan', 'braintree_plan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->string('stripe_id')->nullable()->collation('utf8mb4_bin');
            $table->dropColumn('braintree_id');
        });

        Schema::table('subscriptions', function ($table) {
            $table->string('stripe_id')->collation('utf8mb4_bin');
            $table->string('stripe_plan');
            $table->dropColumn('braintree_id');
            $table->dropColumn('braintree_plan');
        });

        Schema::table('plans', function ($table) {
            $table->renameColumn('braintree_plan', 'stripe_plan');
        });
    }
}
