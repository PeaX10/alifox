<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('categories', function(Blueprint $table){
            $table->foreign('parent_id')->references('id')->on('categories');
        });

        Schema::table('product_details', function(Blueprint $table){
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::table('products', function(Blueprint $table){
            $table->foreign('product_detail_id')->references('id')->on('product_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function(Blueprint $table){
            $table->dropForeign('user_id');
        });

        Schema::table('categories', function(Blueprint $table){
            $table->dropForeign('parent_id');
        });

        Schema::table('product_details', function(Blueprint $table){
            $table->dropForeign('product_id');
        });

        Schema::table('products', function(Blueprint $table){
            $table->dropForeign('product_detail_id');
        });
    }
}
