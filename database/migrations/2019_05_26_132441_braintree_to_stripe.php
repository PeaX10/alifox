<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BraintreeToStripe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->renameColumn('braintree_id', 'stripe_id');
            $table->renameColumn('braintree_plan', 'stripe_plan');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('braintree_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->renameColumn('stripe_id', 'braintree_id');
            $table->renameColumn('stripe_plan', 'braintree_plan');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('braintree_id');
        });
    }
}
