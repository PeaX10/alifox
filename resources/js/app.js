import VModal from 'vue-js-modal'
window.Vue = require('vue');
Vue.use(require('vue-resource'));
Vue.use(VModal, { dynamic: true, injectModalsContainer: true });

var products = Vue.component('products', require('./components/ProductsComponent.vue').default);
var product = Vue.component('product', require('./components/ProductComponent.vue').default);
var infobox = Vue.component('infobox', require('./components/InfoboxComponent.vue').default);
var infiniteLoading = Vue.component('InfiniteLoading', require('vue-infinite-loading'));

const app = new Vue({
    el: '#app',
});