@extends('layouts.app', ['footer' => true])

@section('title', 'AliFox - Privacy')

@section('content')
<section class="my-4 p-5">
    <div class="container text-large">
        <h2>Affiliates</h2>
        <p>If you enjoyed Alifox and want to share it with your audience, become one of our exclusive affiliates and start making money.</p>
        <p>Send us a message using the form below and tell us how do you plan to promote Alifox. Our affiliate manager will come back to you within 48 hours.</p>
        <hr>
        @if(Session::has('mail_sended_success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Congratulations!</strong> Your message has been sent, thank you!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @elseif(Session::has('mail_sended_error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Oups!</strong> an error occurred while sending the mail
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        {!! Form::open() !!}
        {!! Form::text('name')->placeholder('Name') !!}
        {!! Form::text('email')->placeholder('Email')->type('email') !!}
        {!! Form::textarea('message')->placeholder('Your message') !!}
        {!! Form::submit('Send !')->attrs([ 'class' => 'btn btn-fox btn-lg']) !!}
        {!! Form::close() !!}

    </div>
</section>
@endsection