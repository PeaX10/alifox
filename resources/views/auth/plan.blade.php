@extends('layouts.app', ['footer' => true])

@section('css')
    <link rel="stylesheet" href="{{ url('css/stripe.css') }}">
@endsection

@section('content-fluid')
    <style>
        @media (min-width: 992px){
            html, body{
                min-height: 100%;
            }

            @media (min-height: 850px){
                html, body {
                    height: 100%;
                }
            }
        }
    </style>
    <div class="container-fluid h-100 p-0">
        <div class="row justify-content-center h-100">
            <div class="col-lg-6 hidden-md-down bg-white border-right p-5 shadow-right">
                <div class="col-lg-10 offset-lg-2 pr-lg-5">
                    <form action="{{ route('payment.create', ['plan' => $plan]) }}" method="post" id="payment-form">
                        <div id="card-errors" class="alert alert-danger">

                        </div>
                        @csrf
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header collapsed bg-white" data-toggle="collapse" href="#stripe">
                                    <div class="row px-2">
                                        <div class="col-1 text-right">
                                            <input class="form-check-input" type="radio" name="paymentMethod" id="stripeRadio" value="stripe" checked>
                                        </div>
                                        <div class="col-11">
                                            <b class="align-middle">Credit Card</b>
                                            <div class="ml-auto d-inline">
                                                <img src="{{ url('img/payment/ccs.png') }}" alt="Credit Card" class="float-right" style="max-width: 175px">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="stripe" class="card-body collapse show bg-light" data-parent="#accordion" >
                                    <div class="cell payment" id="payment">
                                        <div class="fieldset">
                                            <div id="card-number" class="field empty"></div>
                                            <div id="card-expiry" class="field empty half-width"></div>
                                            <div id="card-cvc" class="field empty half-width"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-header collapsed bg-white" data-toggle="collapse" data-parent="#accordion" href="#paypal">
                                    <div class="row px-2">
                                        <div class="col-1 text-right">
                                            <input class="form-check-input" type="radio" name="paymentMethod" id="paypalRadio" value="paypal">
                                        </div>
                                        <div class="col-11">
                                            <img src="{{ url('img/payment/paypal.png') }}" alt="Paypal" style="max-width: 100px">
                                        </div>
                                    </div>
                                </div>
                                <div id="paypal" class="card-body collapse bg-light" data-parent="#accordion" >
                                    <button class="btn btn-outline-primary" disabled><i class="fab fa-paypal"></i> Comming soon</button>
                                </div>
                            </div>
                        </div>


                        <div id="loading" class="text-center py-3" style="display:none">
                            <div class="spinner-border text-center text-fox" style="width: 4rem; height: 4rem;" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <a href="{{ route('goPro') }}" class="text-fox">< Return to plans</a>
                            <button type="submit" id="pay" class="btn btn-fox ml-auto">
                                {{ __('Complete order') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-6 bg-light p-5">
                <div class="pl-5">
                    <h3>PRO Version</h3>
                    <h3 class="text-muted font-weight-light">{{ $plan->name }} Plan</h3>
                    <br>
                    <br>
                    <h5>Total : <b>${{ $plan->cost }} / {{ $plan->period }}</b></h5>
                    <br>

                    <img src="{{ url('img/payment/mcafee-norton.png') }}" alt="McAfee & Norton" style="max-width: 250px;">
                </div>
            </div>
        </div>
@endsection

@section('js')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
      var stripe = Stripe('{{ env('STRIPE_KEY') }}');

      var elements = stripe.elements({
          fonts: [
            {
              cssSrc: 'https://fonts.googleapis.com/css?family=Quicksand',
            },
          ],
          // Stripe's examples are localized to specific languages, but if
          // you wish to have Elements automatically detect your user's locale,
          // use `locale: 'auto'` instead.
          locale: window.__exampleLocale,
        });

        var elementStyles = {
          base: {
            color: '#000',
            fontWeight: 600,
            fontFamily: 'Quicksand, Open Sans, Segoe UI, sans-serif',
            fontSize: '16px',
            fontSmoothing: 'antialiased',

            ':focus': {
              color: '#424770',
            },

            '::placeholder': {
              color: '#9BACC8',
            },

            ':focus::placeholder': {
              color: '#CFD7DF',
            },
          },
          invalid: {
            color: '#fff',
            ':focus': {
              color: '#FA755A',
            },
            '::placeholder': {
              color: '#FFCCA5',
            },
          },
        };

        var elementClasses = {
          focus: 'focus',
          empty: 'empty',
          invalid: 'invalid',
        };

        var cardNumber = elements.create('cardNumber', {
          style: elementStyles,
          classes: elementClasses,
        });
        cardNumber.mount('#card-number');

        var cardExpiry = elements.create('cardExpiry', {
          style: elementStyles,
          classes: elementClasses,
        });
        cardExpiry.mount('#card-expiry');

        var cardCvc = elements.create('cardCvc', {
          style: elementStyles,
          classes: elementClasses,
        });
        cardCvc.mount('#card-cvc');

        var form = document.getElementById('payment-form');
        var errorElement = document.getElementById('card-errors');
        errorElement.style.display = 'none';

        form.addEventListener('submit', function(event) {
          event.preventDefault();
          errorElement.style.display = 'none';

          stripe.createToken(cardNumber).then(function(result) {
            if (result.error) {
              errorElement.textContent = result.error.message;
              errorElement.style.display = 'block';
            } else {
              stripeTokenHandler(result.token);
            }
          });
        });

      function stripeTokenHandler(token) {
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        form.submit();
      }
    </script>
    <script>
        $('.card-header').click(function(){
            $(this).find('input[type="radio"]').prop("checked", true);
        });
        $('a[data-toggle="collapse"]').click(function(){
          $(this).find('input[type="radio"]').prop("checked", true);
        });

    </script>
@endsection

