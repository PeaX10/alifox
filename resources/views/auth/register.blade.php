@extends('layouts.app', ['footer' => true])

@section('content')
    <div class="container py-5">
        <div class="col-md-8 offset-md-2">
            <h1 class="font-weight-bold text-center pb-5">{{ __('Get started with your FREE account') }}</h1>
            {!!Form::open() !!}
            {!!Form::text('name')->placeholder('Name') !!}
            {!!Form::text('email')->placeholder('E-mail')->type('email') !!}
            {!!Form::text('password')->placeholder('Password')->type('password') !!}
            {!!Form::text('password_confirmation')->placeholder('Confirm password')->type('password') !!}
            {!!Form::checkbox('terms', 'I accept the <a href="'.route('terms').'">Terms of Use</a> and the <a href="'.route('privacy').'">Privacy Policy</a>')!!}
            <hr>
            <div class="form-group row">
                <button type="submit" class="btn-lg btn-fox m-auto text-uppercase">
                    {{ __('Create account') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
