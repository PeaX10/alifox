@extends('back.layout.default')

@section('title', 'Admin AliFox')


@section('content')

    <div class="card">
        <div class="card-header">
            Utilisateur
        </div>
        <div class="card-body">
            <h3>Total : {{ $users->count() }}</h3>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Nbr</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($usersByDays as $date => $users)
                        <tr>
                            <td>{{ $date }}</td>
                            <td>{{ $users->count() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection