@extends('layouts.app', ['footer' => true])

@section('title', 'AliFox - Privacy')

@section('content')
<section class="my-4 p-5">
    <div class="container text-large pb-5">
        <h2>Contact</h2>

        <p>Feel free to use the form below for any question or suggestion you may have.</p>
        <hr>
        @if(Session::has('mail_sended_success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Congratulations!</strong> Your message has been sent, thank you!
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @elseif(Session::has('mail_sended_error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Oups!</strong> an error occurred while sending the mail
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        {!! Form::open() !!}

        {!! Form::text('name')->placeholder('Name') !!}
        {!! Form::text('email')->placeholder('Email')->type('email') !!}
        {!! Form::textarea('message')->placeholder('Your message') !!}
        {!! Form::submit('Send !')->attrs([ 'class' => 'btn btn-fox btn-lg float-right']) !!}
        {!! Form::close() !!}

    </div>
</section>
@endsection