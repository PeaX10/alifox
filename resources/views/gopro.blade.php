@extends('layouts.app', ['footer' => true])

@section('title', 'AliFox - Home')

@section('content')

<section class="jumbotron text-center">
    <h1 class="jumbotron-heading font-weight-bold">Most Affordable, Yet Most Powerful</h1>
    <h1 class="jumbotron-heading font-weight-bold">Winning Products Finder</h1>
</section>

<div class="container pt-5">
    <div class="container-fluid row mx-0">
        <div class="col-lg-4 d-flex mt-5 mt-lg-0">
            <div class="card shadow flex-fill pt-2">
                <div class="card-body text-center d-flex flex-column">
                    <h3 class="card-title">PRO VERSION</h3>
                    <h6 class="card-subtitle mb-2">Billed monthly</h6>
                    <hr class="hr-text text-muted mx-0" data-content="ONLY">
                    <div class="align-bottom mt-auto">
                        <p class="card-text display-4 font-weight-bold">$9.99</p>
                        <p class="card-text text-muted">per month</p>
                        <p class="card-text text-dark text-x-large font-weight-bold">CANCEL ANYTIME</p>
                        @if(Auth::check() && !Auth::user()->subscribed('main'))
                            <a href="{{ route('register.plan', ['plan' => 'month']) }}" class="btn btn-lg btn-outline-dark pl-5 pr-5">CHOOSE</a>
                        @else
                            <a href="{{ route('register', ['plan' => 'month']) }}" class="btn btn-lg btn-outline-dark pl-5 pr-5">SIGN UP</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 d-flex">
            <div class="card shadow flex-fill">
                <div class="bg-fox rounded-top" style="height:0.5rem;"></div>
                <div class="card-body text-center d-flex flex-column">
                    <h3 class="card-title">PRO VERSION <i class="text-fox">(SPECIAL OFFER)</i></h3>
                    <h6 class="card-subtitle mb-2">Billed annually</h6>
                    <hr class="hr-text text-muted mx-0" data-content="ONLY">
                    <div class="align-bottom mt-auto">
                        <p class="card-text text-fox display-3 font-weight-bold">$3.99</p>
                        <h1 class="card-text text-dark font-weight-bold"><del>$7.99</del></h1>
                        <p class="card-text text-muted">per month (total of $47.99/y)</p>
                        <p class="card-text text-dark text-x-large font-weight-bold">CANCEL ANYTIME</p>
                        @if(Auth::check() && !Auth::user()->subscribed('main'))
                            <a href="{{ route('register.plan', ['plan' => 'year']) }}" class="btn btn-lg btn-fox pl-5 pr-5">CHOOSE</a>
                        @else
                            <a href="{{ route('register', ['plan' => 'year']) }}" class="btn btn-lg btn-fox pl-5 pr-5">SIGN UP</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container text-center font-weight-bold my-5">
        <h4>Unlock ALL products & categories</h4>
        <h4>Full data access</h4>
        <h4>Private newsletter</h4>
        <h4>24/7 support</h4>
    </div>
</div>

<section class="bg-light mt-4 p-5">
    <div class="container">
        <h3><i class="far fa-question-circle text-fox"></i> Frequently Asked Questions</h3>
        <div class="p-4 text-large">
            <b>Why should I join Alifox?</b>
            <p>Because you're tired of wasting time and energy scrolling on AliExpress, Ecomhunt & Facebook looking for figures and guesses.
            <br> Because you're tired of wasting hundreds of dollars on testing fake hot products.
            <br>Because you finally want to make real money by finding winning products in a matter of seconds.</p>
            <b class="pt-3">Will I find winning products with Alifox?</b>
            <p>Yes! We don’t look for products and think they could succeed. Our algorithm scans milllions of products on AliExpress, Facebook Ads and Google Ads so it knows which products are currently killing it, in any category or industry. Each product is analyzed and scored depending on newness, total sales, sales per hour, ads/search engagement and on 8 more secret signals.</p>
            <b class="pt-3">How often the Alifox products list is updated?</b>
            <p>Everyday! We, automatically, add / update / remove products and details as soon as we have new information coming from our secret signals.</p>
            <b class="pt-3">Which payment methods are accepted on Alifox?</b>
            <p>We accept Credit/Debit cart using Stripe & Paypal. Both are totally safe and secure.</p>
            <b class="pt-3">What if I want to cancel my subscription?</b>
            <p>You can! Simply go to your Alifox account settings and click on the "Cancel subscription" button. No question asked. No need to send emails.</p>
        </div>
    </div>
</section>
@endsection

@section('modal')
<div class="modal fade" id="howWorkModal" tabindex="-1" role="dialog" aria-labelledby="howWorkModalLabel" aria-hidden="true">
    <div class="modal-dialog w-80" role="document">
        <div class="modal-content">
            <div class="modal-body p-5 text-large">
                <h1>Build your dropshipping business with confidence.</h1>
                <br>
                <p>Stop worrying and wasting time and money searching and testing dozens of so-called hot products.</p>
                <p>Alifox takes that hassle out of the equation by giving you, on a silver plate, all products that are currently killing it. Simply put, Alifox tells you, for any category, which are the most dropshipped products sold RIGHT NOW on AliExpress.</p>
                <br>
                <h2>How ?</h2>
                <div class="row">
                    <ul class="col-md-6 list-unstyled text-left">
                        <li>By NOT doing what other services are doing...</li>
                        <li><i class="fas fa-ban text-danger"></i> We don't handpick products.</li>
                        <li><i class="fas fa-ban text-danger"></i> We don't use our guts or best guesses.</li>
                        <li><i class="fas fa-ban text-danger"></i> We don't spray and pray.</li>
                    </ul>
                    <ul class="col-md-6 list-unstyled text-left">
                        <li><i class="fas fa-check text-success"></i> We use maths.</li>
                    </ul>
                </div>
                <br>
                <p>We built a powerful artificial intelligence algorithm that scans and tracks millions of products on AliExpress, Facebook Ads and Google Ads. If any product starts to sell pretty well, we know it instantly and we'll show it to you.</p>
                <p>So you can take your business to the next level.</p>
            </div>
        </div>
    </div>
</div>
@endsection
