<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ url('css/app.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">
    @yield('css')
    <link rel="icon" type="image/png" href="{{ url('img/favicon.png') }}" />
    <title>@yield('title', 'AliFox')</title>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140087882-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-140087882-1');
    </script>
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '553803775045350');
        fbq('track', 'PageView');
    </script>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-darkblue">
        <div class="container">
            <a class="navbar-brand mr-auto" href="{{ route('home') }}">
                <img src="{{ url('img/logo.png') }}" width="140" height="29" class="d-inline-block align-top" alt="AliFox Logo">
            </a>
            @if($user)
                @if(!$user->subscribed('main'))<a href="{{ route('goPro') }}" class="btn btn-goPro mr-3">
                    <span class="d-none d-sm-block">GO PRO</span>
                    <span class="d-sm-none"><i class="fas fa-star"></i></span>
                </a>@endif
                <div class="btn-group">
                    <button type="button" class="btn @if($user->subscribed('main')) btn-fox @else btn-light @endif dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user-circle pr-2"></i> {{ $user->name }}
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('settings') }}"><i class="fas fa-cog pr-2"></i> Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"><i class="fas fa-power-off pr-2"></i> Logout</a>
                    </div>

                </div>
            @else
                <a href="{{ route('login') }}" data-toggle="modal" data-target="#loginModal" class="p-3 pr-5 text-light nav-item d-none d-sm-block" href="#" >login</a>
                <a href="{{ route('login') }}" data-toggle="modal" data-target="#loginModal" class="p-3 pr-3 text-light nav-item d-sm-none" href="#" ><i class="fas fa-user-circle text-x-large"></i></a>
                <a href="{{ route('register') }}" class="btn btn-goPro text-uppercase">register</a>
            @endif
        </div>
    </nav>
</header>
@yield('content-fluid')
<main role="main" @if(!empty($footer) && $footer)class="mb-5"@endif>
    @yield('content')
</main>

@yield('modal')

@if(!$user)
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-darkblue text-light">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open()->route('login')->post() !!}
            <div class="modal-body">
                {!! Form::text('email')->placeholder('Email')->type('email') !!}
                {!! Form::text('password')->placeholder('Password')->type('password') !!}
                <div class="row">
                    <div class="col-sm">{!!Form::checkbox('remember', 'Remember Me ?')!!}</div>
                    <div class="col-sm">
                        <a href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                </div>
                </div>


            </div>
            <div class="modal-footer">
                {!! Form::submit('Log in')->attrs(['class' => 'btn-fox']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endif

@if(!empty($footer)  && $footer)
    <footer class="bg-darkblue p-0">
        <div class="container text-white py-4">

            <nav class="navbar navbar-expand-lg navbar-dark bg-darkblue">
                <div class="container">
                    <a class="navbar-brand mr-auto" href="{{ route('home') }}">
                        <img src="{{ url('img/logo.png') }}" width="100" height="21" class="d-inline-block align-top" alt="AliFox Logo">
                    </a>
                    <a href="{{ route('privacy') }}" class="p-2 pr-3 text-light nav-item">Privacy</a>
                    <a href="{{ route('terms') }}" class="p-2 pr-3 text-light nav-item">Terms</a>
                    <a href="{{ route('affiliates') }}" class="p-2 pr-3 text-light nav-item">Affiliates</a>
                    <a href="{{ route('contact') }}" class="text-light nav-item">Contact</a>
                </div>
            </nav>
        </div>
        <div class="py-2 text-white text-center">
            <div class="container">
                <p>Copyright © 2019 - AliFox</p>
            </div>
    </footer>
@endif
<script src="{{ url('js/jquery.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
@if( ($errors->has('email') || $errors->has('password')) && Route::currentRouteName() != 'password.request' && Route::currentRouteName() != 'contact' && Route::currentRouteName() != 'register')
    <script>
        $('#loginModal').modal();
    </script>
@endif
@yield('js')
</body>
</html>

