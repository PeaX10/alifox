@extends('layouts.app', ['footer' => true])

@section('title', 'AliFox - Privacy')

@section('content')
<section class="my-4 p-5">
    <div class="container text-large">
        <h2>1 - Scope of this Privacy Policy; Information Collected</h2>

        <p>Any Submission or other information you provide or is otherwise accessible to ALIFOX (including from ancillary services such as Facebook or Google+, as well as Apple or Android application markets, all owned and/or operated by their respective owners, is considered, for the purpose of these Terms and Conditions and this Privacy Policy hereunder, as Personally Identifiable Information.</p>

        <p>This Privacy Policy does not apply to any act or omission of any other person, third party entity or corporation, which ALIFOX does not own or control, or to individuals whom ALIFOX does not employ or manage, including any of the third parties to which ALIFOX may disclose user information as set forth in this Privacy Policy.</p>

        <p>The contents of Submissions that have been delivered by the ALIFOX Service or Application may not necessarily copied, kept or archived by ALIFOX in the normal course of business. You may not rely on ALIFOX in any way with respect to maintaining records of you activity.</p>

        <h2>2 - Use of Information by ALIFOX</h2>

        <p>ALIFOX uses the Personally Identifiable Information you submit to us through the Application or Service, to operate, maintain, and provide to you the features and functionality of the Service.</p>

        <p>ALIFOX may use non-personally- identifiable information (including user usage data, cookies, IP addresses, browser type, clickstream data, etc.) to improve the quality and design of the ALIFOX Application and Service.</p>

        <h2>3 - Collection of Information</h2>

        <p>The Operator complies with web browser’s mechanisms to allow choice. When using the Application, you may choose to stop sharing information at any time using your browser’s settings to disable the Application add-on. This will not erase historic data. For erasure request please email: contact@alifox.co.</p>

        <h2>4 - Use of your Information</h2>

        <p>ALIFOX will not sell or share your Personally Identifiable Information (such as mobile phone number) with other third-party without your consent or except as part of a specific offer or program or feature for which you will have the ability to either opt-in or opt-out, and which may not financially charge you without your consent.</p>

        <p>We may collect personally identifiable information about individual online activities over time and across different websites using the Website, Application and Services.</p>

        <h2>5 - Data Security</h2>

        <p>ALIFOX uses reasonable commercially reasonable physical and technical safeguards to preserve the integrity and security of your personal information. ALIFOX can not and does not ensure or warrant the security of any information or Submission transmitted. Avoid unsecured or unprotected networks to submit messages through the ALIFOX Service.</p>

        <h2>6 - Children's Privacy</h2>

        <p>If you are under 16 years of age, then please do not use the ALIFOX Service or access the ALIFOX Site at any time or in any manner. If ALIFOX learns that Personally Identifiable Information of persons under 16 years of age has been collected using the Service, then ALIFOX may deactivate the account and/or remove the Submissions or render them inaccessible.</p>

        <h2>7 - Event of Merger, Sale, or Liquidation</h2>

        <p>In the event that Operator is acquired by or merged with a third party entity, we reserve the right (or in the event of liquidation, as provided under law) to transfer or assign the information collected from our users as part of such merger, acquisition, sale, or other change of control.</p>


    </div>
</section>
@endsection