@extends('layouts.app', ['footer' => true])

@section('title', 'AliFox - Settings')

@section('content')
<section class="my-4 p-5">
    <div class="container">
        @if(session()->get('subscription_resumed'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Your subscription has now resumed
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(session()->get('subscription_swaped'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Your subscription has been changed
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(session()->get('subscription_cancelled'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                Your subscription is now canceled
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(session()->get('account_edited'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Your account has been edited
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <h2>Settings</h2>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link text-fox active" id="v-pills-account-tab" data-toggle="pill" href="#v-pills-account" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="fas fa-user-circle"></i> Account</a>
                    <a class="nav-link text-fox" id="v-pills-subscription-tab" data-toggle="pill" href="#v-pills-subscription" role="tab" aria-controls="v-pills-subscription" aria-selected="false"><i class="fas fa-star"></i> Subscription</a>
                    <a class="nav-link text-fox" id="v-pills-invoices-tab" data-toggle="pill" href="#v-pills-invoices" role="tab" aria-controls="v-pills-invoices" aria-selected="false"><i class="fas fa-file-invoice-dollar"></i> Invoices</a>
                </div>
            </div>
            <div class="col-md-8 bg-light">
                <div class="tab-content p-2" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-account" role="tabpanel" aria-labelledby="v-pills-account-tab">
                        {!! Form::open()->fill(Auth::user())->autocomplete('off') !!}
                        <h5 class="pt-3">My information</h5>
                        <hr>
                        {!!Form::text('name', 'Name') !!}
                        {!!Form::text('email', 'Email')->type('email') !!}

                        <h5 class="pt-3">Change your password</h5>
                        <hr>
                        {!!Form::text('password', 'New password')->placeholder('Password')->type('password')->autocomplete('off') !!}
                        {!!Form::text('password_confirmation', 'Confirm new password')->placeholder('Confirm password')->type('password')->autocomplete('off') !!}
                        {!! Form::submit('Update')->attrs(['class' => 'btn-fox']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="tab-pane fade" id="v-pills-subscription" role="tabpanel" aria-labelledby="v-pills-subscription-tab">

                        @if($user->subscribed('main'))
                            <h5 class="pt-3">My Plan</h5>
                            <hr>
                            <span class="badge badge-dark text-large pb-2">{{ App\Plan::where('stripe_plan', $user->subscription('main')->stripe_plan)->firstOrFail()->name }}</span>
                            @if($user->subscription('main')->cancelled())
                                <p>Your subscription will end the: <b>{{ $user->endSubscription()->format('Y-m-d') }}</b></p>
                                @if($user->subscription('main')->onGracePeriod())
                                    <a href="" class="btn btn-danger" data-toggle="modal" data-target="#resumeModal"><i class="fas fa-sync"></i> Resume</a>
                                @elseif($user->subscription('main')->ended())
                                    <p>You have no subscription for this moment</p>
                                    <a href="{{ route('goPro') }}" class="btn btn-goPro">Go Pro</a>
                                @endif
                            @else
                                <p>Your subscription will automatically renew on: <b>{{ $user->endSubscription()->format('Y-m-d') }}</b></p>
                                <a href="" class="btn btn-danger" data-toggle="modal" data-target="#cancelModal"><i class="far fa-stop-circle"></i> Cancel subscription</a>
                            @endif

                            <h5 class="pt-5">Change your plan ?</h5>
                            <hr>
                            @foreach( App\Plan::all() as $plan)
                                @if($user->subscribedToPlan($plan->stripe_plan, 'main'))
                                    @if($plan->period == 'month')
                                        <button data-toggle="modal" data-target="#swapModal" class="btn btn-fox">Switch to Annually Plan</button>
                                    @else
                                        <button data-toggle="modal" data-target="#swapModal" class="btn btn-fox">Switch to Monthly Plan</button>
                                    @endif
                                @endif
                            @endforeach
                        @else
                            <p>You have no subscription for this moment</p>
                            <a href="{{ route('goPro') }}" class="btn btn-goPro">Go Pro</a>
                        @endif
                    </div>
                    <div class="tab-pane fade" id="v-pills-invoices" role="tabpanel" aria-labelledby="v-pills-invoices-tab">
                        @if($user->subscribed('main') && count($user->invoices()) > 0)
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">Date</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Download</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($user->invoices() as $invoice)
                                        <tr>
                                            <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                                            <td>{{ $invoice->total() }}</td>
                                            <td><a href="{{ route('settings.downloadInvoice', ['invoice' => $invoice->id]) }}" class="btn btn-sm btn-dark">Download</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <p>There is no invoices available for this moment</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@section('modal')
    <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Cancel Subscription</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Are you sure you want to cancel your subscription?</h5>
                    <hr>
                    <p>Your PRO access will continue to run until next billing period when it will be canceled automatically.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <a class="btn btn-danger" href="{{ route('settings.subscription.cancel') }}">Yes, cancel please</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="resumeModal" tabindex="-1" role="dialog" aria-labelledby="resumeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-fox text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Resume Subscription</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <a class="btn btn-fox" href="{{ route('settings.subscription.resume') }}">Resume subscription !</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="swapModal" tabindex="-1" role="dialog" aria-labelledby="resumeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-fox text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Change Subscription</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @foreach( App\Plan::all() as $plan)
                        @if($user->subscribedToPlan($plan->stripe_plan, 'main'))
                            @if($plan->period == 'month')
                                <p>Special Promotion : Annual Plan for only $47.99 ($3.99/m instead of $9.99/m with Monthly Plan)</p>
                            @else
                                <p>Are you sure ?</p>
                            @endif
                        @endif
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    @foreach( App\Plan::all() as $plan)
                        @if($user->subscribedToPlan($plan->stripe_plan, 'main'))
                            @if($plan->period == 'month')
                                <a href="{{ route('settings.subscription.swap', ['plan' => 'year']) }}" class="btn btn-fox">Switch to Annually Plan</a>
                            @else
                                <a href="{{ route('settings.subscription.swap', ['plan' => 'month']) }}" class="btn btn-fox">Switch to Monthly Plan</a>
                            @endif
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection