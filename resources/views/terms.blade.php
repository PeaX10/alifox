@extends('layouts.app', ['footer' => true])

@section('title', 'AliFox - Terms')

@section('content')
    <section class="my-4 p-5">
        <div class="container text-large">
            <h2>1 - Acceptance</h2>

            <p>By installing and/or using ALIFOX website: www.alifox.co (“Website”) or any other application or functionality we offer online (collectively, “Service”) you accept and agree to the following terms and conditions. If you do not agree with any of these terms and conditions, you must immediately cease all use of the Services, the Website or the Application.</p>

            <p>These Terms and Conditions shall govern any use you make of the Service " and any dispute in connection therewith, and are deemed a binding agreement between you and HERCULE MEDIA SAS. (“Operator” or “ALIFOX”, interchangeably).</p>

            <h2>2 - Services</h2>

            <p>Our Website offers a curated collection of trending products sold online and related information. If you wish to point out a product which can be a great business opportunity, you should submit it to contact@alifox.co. This information is collectively referred to as “BI Content”.</p>

            <p>The information gathered by the Application (as defined below) and in the Facebook group is collectively referred to as "User Submitted Content”.</p>

            <p>Both BI Content and User Submitted Content may cite or refer to third party content, including screenshots, statistics and links to third party websites or services (“Third Party Content”). You may only submit Third Party Content with permission from the respective owner. We do not imply any affiliation with Third Party Content or other websites or services.</p>

            <p>(The User Submitted Content and the BI Content, exclusive of any and all Third-Party Content, are collectively referred to as our “Content”).</p>

            <p>PLEASE NOTE:</p>

            <p>WE ARE NOT RESPONSIBLE FOR BI CONTENT, USER-SUBMITTED DATA, OR THIRD PARTY PRODUCTS AND PROMOTIONS IN ANY WAY.</p>

            <p>WE DO NOT IMPLY ANY AFFILIATION WITH PRODUCT MANUFACTURERS, FACEBOOK, ALI EXPRESS, OR ANY OTHER THIRD PARTY.</p>

            <p>THE WEBSITE’S DESIGN, “LOOK AND FEEL”, GRAPHIC AND MULTIMEDIA ELEMENTS, AND THE STRUCTURE, SEQUENCE AND ORGANIZATION OF THE WEBSITE ARE SOLE PROPERTY OF THE OPERATOR.</p>

            <p>THE APPLICATION AND THE WEBSITE ALL TRADE NAMES AND TRADE MARKS BELONG TO THEIR</p>

            <p>RESPECTIVE OWNERS.</p>

            <p>The Website, the Application and all Services provided in connection therewith are provided “AS-IS” and any and all warranties in connection therewith is hereby disclaimed to the maximum extent allowed under law.</p>

            <p>These Terms of Service apply to all users of the ALIFOX Service. Information provided by our users through the ALIFOX Service may contain (a) indecent, harmful, malicious, defamatory or otherwise unwanted or even illegal content; (b) links to third party services, websites or content which is not regulated in any way by the Operator; (c) commercial content, publications, offers or other information. Operator assumes no responsibility for the services, products, content, or interaction with any third-party websites or services.</p>

            <p>For Removal Requests, please see Section 9 below.</p>

            <p>By using the Application, you expressly acknowledge and agree that Operator shall not be liable in any way whatsoever for any damages, claims or any other liability arising from or related to any act or omission by any third party, explicitly including any other user of the ALIFOX Application, and the Oprator’s liability is explicitly disclaimed hereunder for any content delivered (or not delivered) for any reason or intent, via the Application.</p>

            <h2>3 - License</h2>

            <p>Subject to your compliance with these Terms of Service, Operator hereby grants you an international, limited, personal, non-assignable license to use the Application.</p>

            <p>All design, features, graphics, interfaces and multimedia elements, trademarks and service marks remain the sole intellectual property of the Operator (or their respective owner).</p>

            <h2>4 - Subscriptions</h2>

            <p>Subscribers receive full access to the Website, Services and all User Submitted Content. Subscriptions may be terminated at any time by written notice to contact@alifox.co, after up to 3 business days for processing. Services which may depend on third party services and may be suspended at any time.</p>

            <h2>5 - Consulting Services</h2>

            <p>Consulting services May be provided for Subscribers or as an independent service. It is hereby clarified that Consulting services are provided on a “best effort” basis, and subject to availability. Consulting services are provided without any representations or warranties.</p>

            <h2>6 - Your Obligations</h2>

                <h5>6.1. Protection of Rights: You undertake to refrain for any action which may jeopardize any right of the Operator, explicitly including any breach of the license and/or breach of any acceptable use policy, and you shall explicitly refrain from any (a) intentional breach of any right, including any Intellectual Property Right of the Operator, by any unlawful or unauthorized copy, distribution, dissemination, or undue grant of access to the Application, or any action intended to facilitate the same in any way (such as automated access of any sort, reverse-engineering or any alteration or modification of the Application).</h5>

                <h5>6.2. User’s Responsibility: You are and shall remain solely responsible for any and all use of the Application, including all messages and content that you submit or deliver using the Application, including User Submitted Content (“Submissions”), and hold ALIFOX harmless from any claim of infringement of copyright or other intellectual right, or the transmission of harmful or defamatory content of any sort.</h5>

                <h5>6.3. Suspension of Service: Without derogating from any other provision of these Terms and Conditions, ALIFOX is entitled to suspend the Service or any part thereof, at its sole discretion, from any user, without prior notice, including if suspicion of abuse or misuse of the Services, at ALIFOX'ssole discretion, and you explicitly waive any claim with respect to the same.</h5>

            <h2>7 - Submissions</h2>

            <p>You hereby give consent for ALIFOX to publish, transmit and display your information, messages, media, feedback and Submissions to other users. You acknowledge your Submissions may be hosted on remote platforms and servers, visible to devices or others, including unintended recipients of the Application, if not specifically blocked, and hereby absolve and release ALIFOX from any liability and responsibility in connection therewith.</p>

                <h2>8 - Disclaimer of Warranty</h2>

                <h5>8.1. No Warranty with respect to Confidentiality: ALIFOX is only acting as a repository of data, user content and Submissions do not necessarily represent the views or opinions of ALIFOX or any person acting on its behalf.

                <h5>8.2. No Warranty: ALIFOX makes no guarantees as to the accuracy, validity, or legal status of your or other users’ Submissions or recommends any third party products .

                <h5>8.3. No Rivalry: You are solely responsible for your Submissions and the consequences of posting or publishing them. ALIFOX shall not be deemed a valid party to dispute between You and another user of the Application.

                    <h2>9 - User Submitted Content</h2>

                <h5>9.1. You affirm, represent, and warrant that you own or have the necessary licenses, rights, consents, and permissions to your Submissions, including the right to grant ALIFOX permission are provided under these Terms and Conditions.

                <h5>9.2. Submissions may include specific information, media files, personal information, contacts and other data, including location-based information.

                <h5>9.3. ALIFOX shall in no event be responsible or liable under any legal doctrine with respect to reliance or results of any aspect of the Service.

                    <h5>9.4. By submitting a Submission to ALIFOX, you grant ALIFOX and any recipient of your Submission a worldwide, non-exclusive, royalty-free, sublicense able and transferable license to use, reproduce, distribute, prepare derivative works of, display, and perform the submissions in connection with the Service, including without limitation for promoting and redistributing part or all of the Service (and derivative works thereof) in any media, format or channel.</h5>

                    <h5>9.5. You retain your ownership rights in your Submissions.</h5>
                </h5>
                    <h2>10 - Removal Requests; Takedown Notice</h2>

                    <h5>10.1. We immediately remove allegedly infringing Submissions and User Submitted Content following a proper takedown notice, corresponding with US Digital Millennium Copyright Act (DMCA) to avoid copyright infringement.</h5>
                </h5>
                    <h5>10.2. Any communication disallowed under these Terms and Conditions, and any threatening, tortuous, harassing, hateful, racially or ethnically offensive, or encourages conduct that would be considered a criminal offense, give rise to civil liability, violate any law, or is otherwise inappropriate or infringes or may reasonably considered infringing of third party rights, is disallowed.</h5>

                    <h5>10.3. ALIFOX may remove any content if properly notified that such content or Submission infringes on another's rights (including intellectual property rights).</h5>

                    <h5>10.4. To file a copyright infringement notification, send a written notice (“Takedown Notice”) that includes the following: (i) Contact details and physical or electronic signature of a person authorized to act on behalf of the owner of the allegedly infringed rights; (ii) Identification of the copyrighted work claimed to have been infringed; (iii) Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit the service provider to locate the material; (iv) a statement that the complaining party has a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and (v) a statement that the information in the notification is accurate, and under penalty of perjury, that the complaint is justified. Such takedown notices may be emailed to contact@alifox.co.</h5>
                ALIFOX reserves the right to remove content and Submissions without prior notice, and may remove them permanently even if the dispute between the infringing and complaining party has been resolved.

                    <h2>11 - Warranty Disclaimer</h2>

                    <p>YOUR USE OF THE SERVICE SHALL BE AT YOUR SOLE RISK. TO THE FULLEST EXTENT PERMITTED BY LAW, ALIFOX DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, IN CONNECTION WITH THE SERVICE AND YOUR USE THEREOF.</p>

                    <p>ALIFOX EXPLICITLY SHALL NOT BE LIABLE FOR ANY DAMAGES, LOSSES, CLAIMS, INFRINGEMENTS OR DEMANDS WHICH MAY ARISE, UNDER ANY LAW, CONTRACT AND/OR LEGAL DOCTRINE, IN CONNECTION OR WITH RESPECT TO ANY ASPECT OF THE SERVICE AND/OR APPLICATION, INCLUDING, WITHOUT LIMITATION, ANY INDIRECT, CONSEQUENTIAL, EXEMPLARY, PUNITIVE, SPECIAL, THEORETICAL (INCLUDING LOSS OF GOODWILL OR LOSS OF PROFITS OF ANY SORT) OR INCIDENTAL DAMAGE OR LOSS OF ANY SORT.</p>

                    <h2>12 - Limitation of Liability</h2>

                    <p>WITHOUT DEROGATING OF ANY DISCLAIMED LIABILITY, TO THE MAXIMUM EXTENT PERMITABLE BY LAW, IT IS HEREBY AGREED THAT ALIFOX'S LIABILITY SHALL NOT EXCEED, UNDER ANY CIRCUMSTANCES, THE GROSS TOTAL AGGREGATE AMOUNT OF PAYMENTS ACTUALLY PAID AND RECEIVED BY ALIFOX FROM THE USER.</p>

                    <h2>13 - Suspension of Service</h2>

            <p>ALIFOX reserves the right to discontinue the Service or any aspect or feature of the Service, at any time, temporarily or permanently, including for maintenance, service and upgrade purposes.</p>

            <p>The Service is controlled and offered by the Operator from its facilities as may be from time to time. ALIFOX makes no representations that the Service is appropriate or available for use in other locations. Those who access or use the ALIFOX Service from other jurisdictions do so at their own volition and are responsible for compliance with local law.</p>

            <h2>14 - Indemnity</h2>

            <p>You agree to defend, indemnify and hold the Operator harmless, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees) arising from your Submissions and any other aspect of your use (or abuse) and access of the Service; or any violation of these Terms and Conditions; or any third party claim with respect to the same.</p>

            <h2>15 - Ability to Accept Terms of Service</h2>

            <p>You affirm that you are over 16 years of age and otherwise eligible in all respects to engage in this agreement with ALIFOX. Users under the age of 16 may use the Services only under the supervision of their parent, natural or legal guardians, whom must review and accept the terms of use.</p>

            <h2>16 - Assignment</h2>

            <p>These Terms of Service, and any rights and licenses granted hereunder, may not be transferred or assigned by the user. The Operator may assign any of the foregoing and any right or obligation hereunder, without restriction.</p>

            <h2>17 - General</h2>

            <p>You agree that: (i) the Service shall be deemed solely based in France and any issue of dispute with respect to it shall be solely governed and resolved in accordance with the laws of France. (ii) the competent courts in Nice, France, shall have sole jurisdiction with respect to any issue or dispute in connection therewith. (iii) any cause of action not commenced by any party hereunder shall be permanently time-barred one (1) year after it occured.</p>

            <h2>18 - No waiver</h2>

            <p>Operator's failure to assert any right or remedy under these Terms of Service shall not constitute a waiver of such right or remedy.</p>

            <h2>19 - Changes and updates to these terms and Conditions and Privacy Notice</h2>

            <p>These Terms and Conditions may be revised periodically and this will be reflected by the "effective date" below. Please revisit this page to stay aware of any changes. Your continued use of the Website, Application and Services constitutes your agreement to these Terms and conditions and the Privacy Policy and any amendments.</p>

            <h2>20 - Severability and Mandatory Law</h2>

                <p>These Terms and Conditions are severable and shall remain binding, but remain subject to any and all applicable mandatory legislation.</p>
        </div>
    </section>
@endsection