<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Invoice AliFox</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        @import url('https://fonts.googleapis.com/css?family=Lato');
        body {
            font-family: 'Lato', sans-serif;
            background: #fff;
            font-size: 12px;
            line-height: 12px;
            color: #333333;
        }
        h2 {
            font-size:18px;
        }
        .container {
            padding-top:30px;
        }
        .invoice-head td {
            padding: 0 8px;
        }
        .table th {
            vertical-align: bottom;
            font-weight: bold;
            padding: 8px;
            line-height: 20px;
            text-align: left;
        }
        .table td {
            padding: 8px;
            line-height: 20px;
            text-align: left;
            vertical-align: top;
            border-top: 1px solid #dddddd;
        }
        tr.border-bottom td {
            border-bottom:1px solid #4288CE;
        }
    </style>
</head>

<body>
<div class="container">
    <table style="margin-left: auto; margin-right: auto" width="100%">
        <tr>
            <td width="50%">
                &nbsp;{{ $invoice->date()->toFormattedDateString() }}
            </td>
            <td align="left">
                <strong>AliFox Invoice</strong>
            </td>
        </tr>
    </table>
    <div style="margin-top:50px; width: 90%; margin-left:auto; margin-right:auto">
        <strong>AliFox Company - Hercule Media</strong>
        Address line 1<br>
        Address line 2<br>
        Paris, 75001<br>
        VAT ID : <br>
        alifox.co@gmail.com<br>
        <br>
        <h2>Invoice for {{ $owner->email ?: $owner->name }}</h2>
        <br>
        <table style="margin-left: auto; margin-right: auto" width="100%">
            <tr valign="top" class="border-bottom" style="font-weight: bold;">
                <td>
                    Recipient
                </td>
                <td>
                    Date
                </td>
                <td align="right">
                    Invoice Number
                </td>
            </tr>

            <tr valign="top">
                <td>
                    {{ $owner->email ?: $owner->name }}
                </td>
                <td>
                    {{ $invoice->date()->toFormattedDateString() }}
                </td>
                <td align="right">
                    {{ $invoice->id }}
                </td>
            </tr>
        </table>
        <br>
        <table style="margin-left: auto; margin-right: auto" width="100%">
            <tr valign="top" class="border-bottom" style="font-weight: bold">
                <td>
                    Product
                </td>
                <td align="right">
                    Cost
                </td>
            </tr>

            <tr>
                <td>
                    {{ $product }}
                    @if ($invoice->planId)
                        Subscription To "{{ $invoice->planId }}"
                    @elseif (isset($invoice->customFields['description']))
                        {{ $invoice->customFields['description'] }}
                    @else
                        Charge
                    @endif
                </td>
                <td align="right">
                    {{ $invoice->subtotal() }}
                </td>
            </tr>
            @if ($invoice->hasAddOn())
            <tr>
                <td>Add-Ons ({{ implode(', ', $invoice->addOns()) }})</td>
                <td align="right">{{ $invoice->addOn() }}</td>
            </tr>
            @endif

            @if ($invoice->hasDiscount())
            <tr>
                <td>Discounts ({{ implode(', ', $invoice->coupons()) }})</td>
                <td align="right">-{{ $invoice->discount() }}</td>
            </tr>
            @endif

            <tr>
                <td><strong>Total</strong></td>
                <td align="right"><strong>{{ $invoice->total() }}</strong></td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>