<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Pages
Route::get('/', 'HomeController@index')->name('home');
Route::get('/go-pro', 'HomeController@goPro')->name('goPro');
Route::get('/terms', 'HomeController@terms')->name('terms');
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
Route::get('/affiliates', 'HomeController@affiliates')->name('affiliates');
Route::post('/affiliates', 'MailController@affiliates')->name('affiliates.post');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::post('/contact', 'MailController@contact')->name('contact.post');

Route::get('/json/products/{category}', 'ProductController@indexReverse')->name('product.json');
Route::get('/json/categories', 'ProductController@getCategories')->name('category.json');

Route::middleware(['auth'])->group(function () {
    Route::get('/settings', 'SettingController@index')->name('settings');
    Route::post('/settings', 'SettingController@updateUser')->name('settings.post');
    Route::get('settings/invoice/{invoice}', 'SettingController@downloadInvoice')->name('settings.downloadInvoice');
    Route::get('settings/subscription/cancel', 'SettingController@cancelSubscription')->name('settings.subscription.cancel');
    Route::get('settings/subscription/resume', 'SettingController@resumeSubscription')->name('settings.subscription.resume');
    Route::get('settings/subscription/swap/{plan}', 'SettingController@swapSubscription')->name('settings.subscription.swap');
});

// Stripe & Plan
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::get('register/plan/{plan}', 'PaymentController@choosePlan')->name('register.plan');
Route::post('/payment/subscribe/{plan}', 'PaymentController@create')->name('payment.create');
Route::post(
    'stripe/webhook',
    '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook'
);

// ADMIN
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function(){
    Route::get('/', 'Admin\HomeController@index')->name('index');
});